package org.sergiiz.pricingbasket.ui.util;

import org.junit.Test;

import java.util.Currency;
import java.util.Locale;

import static org.junit.Assert.*;

public class PriceFormatterTest {

    private final static String GBP_SYMBOL = Currency.getInstance(Locale.UK).getSymbol();

    @Test
    public void format_Should_ReturnExpected_When_NoDecimals() {
        assertEquals("10,00 GBP", PriceFormatter.format(10, "GBP"));
    }

    @Test
    public void format_Should_ReturnExpected_When_Decimals() {
        assertEquals("10,24 GBP", PriceFormatter.format(10.24, "GBP"));
    }

    @Test
    public void format_Should_ReturnExpected_When_DecimalsCelling() {
        assertEquals("10,25 GBP", PriceFormatter.format(10.2456, "GBP"));
    }

    @Test
    public void format_Should_ReturnExpected_When_Zero() {
        assertEquals("0,00 GBP", PriceFormatter.format(0, "GBP"));
    }

    @Test
    public void formatGbpSymbol_Should_ReturnExpected_When_NoDecimals() {
        assertEquals(GBP_SYMBOL + "10,00", PriceFormatter.formatGbpSymbol(10));
    }

    @Test
    public void formatGbpSymbol_Should_ReturnExpected_When_Decimals() {
        assertEquals(GBP_SYMBOL + "10,24", PriceFormatter.formatGbpSymbol(10.24));
    }

    @Test
    public void formatGbpSymbol_Should_ReturnExpected_When_DecimalsCelling() {
        assertEquals(GBP_SYMBOL + "10,25", PriceFormatter.formatGbpSymbol(10.2456));
    }

    @Test
    public void formatGbpSymbol_Should_ReturnExpected_When_Zero() {
        assertEquals(GBP_SYMBOL + "0,00", PriceFormatter.formatGbpSymbol(0));
    }

}