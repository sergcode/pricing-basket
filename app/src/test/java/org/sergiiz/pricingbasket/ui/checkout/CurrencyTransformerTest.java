package org.sergiiz.pricingbasket.ui.checkout;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class CurrencyTransformerTest {
    @Test
    public void transform_Should_ReturnExpectedValue() throws Exception {
        Map<String, Double> currencyMap = new HashMap<>();
        currencyMap.put("AUD", 10d);
        currencyMap.put("USD", 2d);
        CurrencyItem defaultCurrency = new CurrencyItem("XYZ", 1);

        List<CurrencyItem> expectedValues = new ArrayList<>();
        expectedValues.add(defaultCurrency);
        expectedValues.add(new CurrencyItem("AUD", 10d));
        expectedValues.add(new CurrencyItem("USD", 2d));

        assertEquals(expectedValues, CurrencyTransformer.transform(currencyMap, defaultCurrency));
    }

    @Test
    public void transform_Should_ReturnExpectedValue_WhenEmpty() throws Exception {
        Map<String, Double> currencyMap = new HashMap<>();
        CurrencyItem defaultCurrency = new CurrencyItem("XYZ", 1);
        List<CurrencyItem> expectedValues = new ArrayList<>();
        expectedValues.add(defaultCurrency);
        assertEquals(expectedValues, CurrencyTransformer.transform(currencyMap, defaultCurrency));
    }

}