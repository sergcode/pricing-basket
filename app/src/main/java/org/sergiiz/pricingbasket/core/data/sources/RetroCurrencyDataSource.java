package org.sergiiz.pricingbasket.core.data.sources;


import org.sergiiz.pricingbasket.core.domain.currency.CurrencyApi;
import org.sergiiz.pricingbasket.core.domain.currency.CurrencyDataSource;
import org.sergiiz.pricingbasket.core.domain.currency.ExchangeRateResponse;

import rx.Observable;

public class RetroCurrencyDataSource implements CurrencyDataSource {

    private final CurrencyApi currencyApi;

    public RetroCurrencyDataSource(CurrencyApi currencyApi) {
        this.currencyApi = currencyApi;
    }

    @Override
    public Observable<ExchangeRateResponse> getExchangeRate() {
        return currencyApi.getExchangeRate();
    }
}
