package org.sergiiz.pricingbasket.core.domain.currency;

import rx.Observable;

public interface CurrencyDataSource {

    Observable<ExchangeRateResponse> getExchangeRate();

}
