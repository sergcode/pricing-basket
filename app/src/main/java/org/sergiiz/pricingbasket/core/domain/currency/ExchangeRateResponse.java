package org.sergiiz.pricingbasket.core.domain.currency;

import java.util.Map;

public class ExchangeRateResponse {
    public String date;
    public String base;
    public Map<String, Double> rates;

    @Override
    public String toString() {
        return "ExchangeRateResponse{" +
                "date='" + date + '\'' +
                ", base='" + base + '\'' +
                ", rates=" + rates +
                '}';
    }
}
