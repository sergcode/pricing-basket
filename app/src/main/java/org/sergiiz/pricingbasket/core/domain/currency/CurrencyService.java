package org.sergiiz.pricingbasket.core.domain.currency;


import rx.Observable;

public class CurrencyService {
    private final CurrencyDataSource dataSource;

    public CurrencyService(CurrencyDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Observable<ExchangeRateResponse> getExchangeRate(){
        return dataSource.getExchangeRate();
    }
}
