package org.sergiiz.pricingbasket.core.data.sources;

import java.util.Arrays;
import java.util.List;

import org.sergiiz.pricingbasket.core.domain.product.Product;

class ProductProviderScaffolding {

    private ProductProviderScaffolding() {
    }

    public static List<Product> getProductList() {
        Product peas = new Product("sku2", "Peas", "http://servingjoy.com/wp-content/uploads/2015/04/Fun-Facts-of-Peas.jpg", 0.95);
        Product eggs = new Product("sku1", "Eggs", "https://pioneerwoman.files.wordpress.com/2015/10/perfect-easy-to-peel-hard-boiled-eggs-02.jpg", 2.10);
        Product milk = new Product("sku3", "Milk", "https://www.bbcgoodfood.com/sites/default/files/milk-500_0.jpg", 1.30);
        Product beans = new Product("sku4", "Beans", "http://www.theprairiehomestead.com/wp-content/uploads/2013/04/beans.jpg", 0.73);
        return Arrays.asList(peas, eggs, milk, beans);
    }
}
