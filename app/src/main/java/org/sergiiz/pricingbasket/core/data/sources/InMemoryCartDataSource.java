package org.sergiiz.pricingbasket.core.data.sources;

import org.sergiiz.pricingbasket.core.data.util.TimeDelayer;
import org.sergiiz.pricingbasket.core.domain.cart.Cart;
import org.sergiiz.pricingbasket.core.domain.cart.CartBuilder;
import org.sergiiz.pricingbasket.core.domain.cart.CartDataSource;
import org.sergiiz.pricingbasket.core.domain.cart.CartProduct;
import rx.Observable;

public class InMemoryCartDataSource implements CartDataSource {

    private final TimeDelayer timeDelayer;
    private Cart cart;

    public InMemoryCartDataSource(TimeDelayer timeDelayer) {
        this.timeDelayer = timeDelayer;
        this.cart = Cart.EMPTY;
    }

    @Override
    public Observable<Cart> getCart() {
        return Observable.defer(() -> {
            timeDelayer.delay();
            return Observable.just(cart);
        });
    }

    @Override
    public Observable<Cart> addProduct(CartProduct cartProduct) {
        return Observable.defer(() -> {
            timeDelayer.delay();

            cart = CartBuilder.from(cart)
                    .addProduct(cartProduct)
                    .build();

            return Observable.just(cart);
        });
    }

    @Override
    public Observable<Cart> removeProduct(CartProduct cartProduct) {
        return Observable.defer(() -> {
            timeDelayer.delay();

            cart = CartBuilder.from(cart)
                    .removeProduct(cartProduct)
                    .build();

            return Observable.just(cart);
        });
    }
}
