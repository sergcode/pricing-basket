package org.sergiiz.pricingbasket.core.data.sources;

import java.util.List;

import org.sergiiz.pricingbasket.core.data.util.TimeDelayer;
import org.sergiiz.pricingbasket.core.domain.product.Product;
import org.sergiiz.pricingbasket.core.domain.product.ProductDataSource;
import rx.Observable;

public class InMemoryProductDataSource implements ProductDataSource {

    private final TimeDelayer timeDelayer;

    public InMemoryProductDataSource(TimeDelayer timeDelayer) {
        this.timeDelayer = timeDelayer;
    }

    @Override
    public Observable<List<Product>> getAllCatalog() {
        return Observable.defer(() -> {
            timeDelayer.delay();
            List<Product> products = ProductProviderScaffolding.getProductList();
            return Observable.just(products);
        });
    }
}
