package org.sergiiz.pricingbasket.core.domain.currency;

import retrofit2.http.GET;
import rx.Observable;

public interface CurrencyApi {

    @GET("latest?base=GBP")
    Observable<ExchangeRateResponse> getExchangeRate();
}
