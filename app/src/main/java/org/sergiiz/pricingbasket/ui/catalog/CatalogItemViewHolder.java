package org.sergiiz.pricingbasket.ui.catalog;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import org.sergiiz.pricingbasket.R;
import org.sergiiz.pricingbasket.core.domain.product.Product;
import org.sergiiz.pricingbasket.ui.util.PriceFormatter;

class CatalogItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.catalog_item_imageview)
    ImageView image;

    @BindView(R.id.catalog_item_name)
    TextView name;

    @BindView(R.id.catalog_item_price)
    TextView price;

    private final CatalogListener listListener;

    CatalogItemViewHolder(View view, CatalogListener listListener) {
        super(view);
        this.listListener = listListener;
        ButterKnife.bind(this, view);
    }

    public void bind(Product product) {
        name.setText(product.title);
        price.setText(PriceFormatter.formatGbpSymbol(product.price));
        itemView.setOnClickListener(v -> listListener.onProductClicked(product));

        Picasso.with(itemView.getContext())
                .load(product.imageUrl)
                .centerInside()
                .fit()
                .into(image);
    }
}
