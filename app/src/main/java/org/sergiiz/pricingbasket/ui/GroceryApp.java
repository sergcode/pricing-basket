package org.sergiiz.pricingbasket.ui;

import android.app.Application;

import org.sergiiz.pricingbasket.inject.DefaultDependenciesFactory;
import org.sergiiz.pricingbasket.inject.Injector;

public class GroceryApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Injector.using(new DefaultDependenciesFactory());
    }
}
