package org.sergiiz.pricingbasket.ui.checkout;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class CurrencyTransformer {

    static List<CurrencyItem> transform(Map<String, Double> currencyMap, CurrencyItem defaultCurrency) {
        List<CurrencyItem> currencyList = new ArrayList<>(currencyMap.size() + 1);
        currencyList.add(defaultCurrency);
        for (Map.Entry<String, Double> entry : currencyMap.entrySet()) {
            currencyList.add(new CurrencyItem(entry.getKey(), entry.getValue()));
        }
        return currencyList;
    }
}
