package org.sergiiz.pricingbasket.ui.checkout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.sergiiz.pricingbasket.R;
import org.sergiiz.pricingbasket.core.domain.currency.CurrencyService;
import org.sergiiz.pricingbasket.core.domain.currency.ExchangeRateResponse;
import org.sergiiz.pricingbasket.inject.Injector;
import org.sergiiz.pricingbasket.ui.base.BaseActivity;
import org.sergiiz.pricingbasket.ui.util.PriceFormatter;
import org.sergiiz.pricingbasket.util.RxUtil;

import java.util.List;

import butterknife.BindView;

public class CheckoutActivity extends BaseActivity implements CurrencyChangeListener {

    private static final String ARG_TOTAL_IN_GBP = "total_gbp";
    private static final String ARG_CURR_NAME = "curr_name";
    private static final String ARG_CURR_RATE = "curr_rate";

    public static Intent createIntent(Context context, double totalInGBP) {
        Intent intent = new Intent(context, CheckoutActivity.class);
        intent.putExtra(ARG_TOTAL_IN_GBP, totalInGBP);
        return intent;
    }

    private double totalGbp;

    private CurrencyItem currentCurrency;

    @BindView(R.id.checkout_progressbar)
    ProgressBar progressBar;

    @BindView(R.id.checkout_currency_recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.checkout_total_textview)
    TextView textViewTotal;

    @BindView(R.id.checkout_main_content)
    ViewGroup mainContent;

    private CurrencyService currencyService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currencyService = Injector.currencyService();
        totalGbp = getIntent().getDoubleExtra(ARG_TOTAL_IN_GBP, 0d);
        setupViews();
        if (savedInstanceState == null) {
            showTotalPrice(new CurrencyItem(PriceFormatter.DEFAULT_CURRENCY, 1d));
        } else {
            String currName = savedInstanceState.getString(ARG_CURR_NAME);
            Double currRate = savedInstanceState.getDouble(ARG_CURR_RATE);
            showTotalPrice(new CurrencyItem(currName, currRate));
        }
    }

    @Override
    protected int layoutId() {
        return R.layout.checkout_activity;
    }


    private void setupViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.checkout_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public void onStart() {
        super.onStart();
        showProgressBar();
        addSubscritiption(currencyService.getExchangeRate()
                .compose(RxUtil.applyStandardSchedulers())
                .subscribe(this::onExchangeRateResponse, RxUtil.logError()));
    }

    protected void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        if (currentCurrency != null) {
            icicle.putString(ARG_CURR_NAME, currentCurrency.name);
            icicle.putDouble(ARG_CURR_RATE, currentCurrency.exchangeRate);
        }
    }

    private void showProgressBar() {
        mainContent.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setAdapter(null);
    }

    private void showCurrencies(ExchangeRateResponse exchangeRateResponse) {
        CurrencyAdapter adapter = new CurrencyAdapter(this);
        CurrencyItem defaultCurrency = new CurrencyItem(exchangeRateResponse.base, 1d);
        List<CurrencyItem> transformedCurrencies = CurrencyTransformer.transform(exchangeRateResponse.rates, defaultCurrency);
        adapter.setCurrencyMap(transformedCurrencies);
        recyclerView.setAdapter(adapter);
    }

    private void hideProgressbar() {
        mainContent.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onCurrencyClicked(CurrencyItem currencyItem) {
        currentCurrency = currencyItem;
        showTotalPrice(currencyItem);
    }

    void onExchangeRateResponse(ExchangeRateResponse exchangeRateResponse) {
        Log.d(CheckoutActivity.class.getName(), exchangeRateResponse.toString());
        hideProgressbar();
        showCurrencies(exchangeRateResponse);
    }

    private void showTotalPrice(CurrencyItem currencyItem) {
        textViewTotal.setText(PriceFormatter.format(currencyItem.exchangeRate * totalGbp, currencyItem.name));
    }

}
