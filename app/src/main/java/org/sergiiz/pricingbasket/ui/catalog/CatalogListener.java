package org.sergiiz.pricingbasket.ui.catalog;

import org.sergiiz.pricingbasket.core.domain.product.Product;

interface CatalogListener {
    void onProductClicked(Product product);
}
