package org.sergiiz.pricingbasket.ui.checkout;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import org.sergiiz.pricingbasket.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CurrencyItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.text_currency_name)
    TextView currencyName;

    @BindView(R.id.text_currency_ratio)
    TextView currencyRatio;

    private final CurrencyChangeListener listListener;

    CurrencyItemViewHolder(View view, CurrencyChangeListener listListener) {
        super(view);
        this.listListener = listListener;
        ButterKnife.bind(this, view);
    }

    public void bind(CurrencyItem currencyItem) {
        currencyName.setText(currencyItem.name);
        currencyRatio.setText("" + currencyItem.exchangeRate); // TODO formatting?
        itemView.setOnClickListener(v -> listListener.onCurrencyClicked(currencyItem));
    }
}
