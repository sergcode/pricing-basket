package org.sergiiz.pricingbasket.ui.checkout;

public interface CurrencyChangeListener {
    void onCurrencyClicked(CurrencyItem currencyItem);
}
