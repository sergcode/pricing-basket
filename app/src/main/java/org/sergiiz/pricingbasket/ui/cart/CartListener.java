package org.sergiiz.pricingbasket.ui.cart;

import org.sergiiz.pricingbasket.core.domain.cart.CartProduct;

interface CartListener {
    void onCartProductClicked(CartProduct product);
}
