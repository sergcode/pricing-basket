package org.sergiiz.pricingbasket.ui.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Currency;
import java.util.Locale;

public final class PriceFormatter {

    public static final String DEFAULT_CURRENCY = "GBP";
    private static final String DEFAULT_CURRENCY_SYMBOL = Currency.getInstance(Locale.UK).getSymbol();

    private static final DecimalFormat PRICE_DECIMAL_FORMAT = new DecimalFormat("0.00");

    static {
        PRICE_DECIMAL_FORMAT.setRoundingMode(RoundingMode.CEILING);
    }

    private PriceFormatter() {
    }

    public static String formatGbpSymbol(double price) {
        return format(price, DEFAULT_CURRENCY_SYMBOL, true);
    }

    private static String format(double price, String currency, boolean isReverse) {
        String formattedPrice = PRICE_DECIMAL_FORMAT.format(price);
        if (isReverse) {
            return String.format("%s%s", currency, formattedPrice);
        } else {
            return String.format("%s %s", formattedPrice, currency);
        }
    }


    public static String format(double price, String currency) {
        return format(price, currency, false);
    }
}
