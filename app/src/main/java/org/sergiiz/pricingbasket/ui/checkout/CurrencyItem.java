package org.sergiiz.pricingbasket.ui.checkout;

class CurrencyItem {
    final String name;
    final double exchangeRate;

    CurrencyItem(String name, double exchangeRate) {
        this.name = name;
        this.exchangeRate = exchangeRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CurrencyItem that = (CurrencyItem) o;

        if (Double.compare(that.exchangeRate, exchangeRate) != 0) return false;
        return name != null ? name.equals(that.name) : that.name == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name != null ? name.hashCode() : 0;
        temp = Double.doubleToLongBits(exchangeRate);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
