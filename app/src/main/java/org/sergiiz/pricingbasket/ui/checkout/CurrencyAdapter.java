package org.sergiiz.pricingbasket.ui.checkout;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.sergiiz.pricingbasket.R;

import java.util.ArrayList;
import java.util.List;

public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyItemViewHolder> {

    private final List<CurrencyItem> currencyList;
    private final CurrencyChangeListener listListener;

    public CurrencyAdapter(CurrencyChangeListener listListener) {
        this.listListener = listListener;
        this.currencyList = new ArrayList<>();
    }

    public void setCurrencyMap(List<CurrencyItem> currencyList) {
        this.currencyList.clear();
        this.currencyList.addAll(currencyList);
        this.notifyDataSetChanged();
    }

    @Override
    public CurrencyItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkout_currency_item, parent, false);
        return new CurrencyItemViewHolder(view, listListener);
    }

    @Override
    public void onBindViewHolder(CurrencyItemViewHolder holder, int position) {
        CurrencyItem currencyItem = currencyList.get(position);
        holder.bind(currencyItem);
    }

    @Override
    public int getItemCount() {
        return currencyList.size();
    }

}
