package org.sergiiz.pricingbasket.inject;

import org.sergiiz.pricingbasket.core.data.sources.InMemoryCartDataSource;
import org.sergiiz.pricingbasket.core.data.sources.InMemoryProductDataSource;
import org.sergiiz.pricingbasket.core.data.util.TimeDelayer;
import org.sergiiz.pricingbasket.core.domain.cart.CartDataSource;
import org.sergiiz.pricingbasket.core.domain.cart.CartService;
import org.sergiiz.pricingbasket.core.domain.cart.CartStore;
import org.sergiiz.pricingbasket.core.domain.currency.CurrencyApi;
import org.sergiiz.pricingbasket.core.domain.currency.CurrencyDataSource;
import org.sergiiz.pricingbasket.core.domain.currency.CurrencyService;
import org.sergiiz.pricingbasket.core.data.sources.RetroCurrencyDataSource;
import org.sergiiz.pricingbasket.core.domain.product.ProductDataSource;
import org.sergiiz.pricingbasket.core.domain.product.ProductService;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class DefaultDependenciesFactory implements DependenciesFactory {

    private static final String EXCHANGE_API_ENDPOINT = "http://api.fixer.io/";

    private final TimeDelayer timeDelayer;
    private final CartStore cartStore;

    public DefaultDependenciesFactory() {
        timeDelayer = new TimeDelayer();
        cartStore = new CartStore();
    }

    @Override
    public CartService createCartService() {
        CartDataSource cartDataSource = new InMemoryCartDataSource(timeDelayer);
        return new CartService(cartDataSource, cartStore);
    }

    @Override
    public ProductService createProductService() {
        ProductDataSource productDataSource = new InMemoryProductDataSource(timeDelayer);
        return new ProductService(productDataSource);
    }

    @Override
    public CurrencyService createCurrencyService() {
        CurrencyApi currencyApi = buildApi();
        CurrencyDataSource currencyDataSource = new RetroCurrencyDataSource(currencyApi);
        return new CurrencyService(currencyDataSource);
    }


    private CurrencyApi buildApi() {
        return new Retrofit.Builder()
                .baseUrl(EXCHANGE_API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
                .create(CurrencyApi.class);
    }

    @Override
    public CartStore createCartStore() {
        return cartStore;
    }
}
