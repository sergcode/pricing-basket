package org.sergiiz.pricingbasket.inject;

import org.sergiiz.pricingbasket.core.domain.cart.CartService;
import org.sergiiz.pricingbasket.core.domain.cart.CartStore;
import org.sergiiz.pricingbasket.core.domain.currency.CurrencyService;
import org.sergiiz.pricingbasket.core.domain.product.ProductService;

public interface DependenciesFactory {
    CartService createCartService();

    ProductService createProductService();

    CurrencyService createCurrencyService();

    CartStore createCartStore();
}
