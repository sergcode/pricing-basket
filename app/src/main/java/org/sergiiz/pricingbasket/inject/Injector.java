package org.sergiiz.pricingbasket.inject;

import org.sergiiz.pricingbasket.core.domain.cart.CartService;
import org.sergiiz.pricingbasket.core.domain.cart.CartStore;
import org.sergiiz.pricingbasket.core.domain.currency.CurrencyService;
import org.sergiiz.pricingbasket.core.domain.product.ProductService;

/**
 * Class use as a centralize point for DI.
 */
public class Injector {
    private static Injector INSTANCE;
    private final CartService cartService;
    private final ProductService productService;
    private final CurrencyService currencyService;
    private final CartStore cartStore;

    public Injector(CartService cartService, ProductService productService, CurrencyService currencyService,
                    CartStore cartStore) {
        this.cartService = cartService;
        this.productService = productService;
        this.currencyService = currencyService;
        this.cartStore = cartStore;
    }

    public static void using(DependenciesFactory factory) {
        CartService cartService = factory.createCartService();
        ProductService productService = factory.createProductService();
        CurrencyService currencyService = factory.createCurrencyService();
        CartStore cartStore = factory.createCartStore();
        INSTANCE = new Injector(cartService, productService, currencyService, cartStore);
    }

    private static Injector instance() {
        if (INSTANCE == null) {
            throw new IllegalStateException("You need to setup Inject to use a valid DependenciesFactory");
        }
        return INSTANCE;
    }

    public static CartService cartService() { return instance().cartService; }

    public static ProductService productService() { return instance().productService; }

    public static CurrencyService currencyService() { return instance().currencyService; }

    public static CartStore cartStore() {
        return instance().cartStore;
    }
}
